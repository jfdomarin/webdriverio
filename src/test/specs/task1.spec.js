describe('Tarea 1', () => {

  beforeEach('load URL', async () => {
    await browser.url('https://pastebin.com/')
  })

  it('create new paste', async () => {
    const areaTextCode = await $('#postform-text')
    areaTextCode.setValue('Hello from WebDrive')
    
    const pasteExpiration = await $('#postform-expiration').nextElement()
    await pasteExpiration.click()
    await pasteExpiration.waitForDisplayed({ timeout: 1000 })
    const opcionTenMinutes = await $('li[id$="10M"]' )
    await opcionTenMinutes.click()
    console.log(await pasteExpiration.getValue());

    const pasteName = await $('#postform-name')
    pasteName.setValue('helloweb')

    const createNewPaste = await $('//button[text()="Create New Paste"]')
    createNewPaste.click()

    await browser.pause(3000)
  })

})