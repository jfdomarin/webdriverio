describe('first test suite', () => {

  beforeEach("load URL", async()=>{
    await browser.setWindowSize(1200, 900)
    await browser.url('https://www.epam.com/');
  })

  it('Button Contact US and URL', async() => {
    
    //const buttonContactUs = await $('//div[@class="header__content"]/a[@class="cta-button-ui cta-button-ui-23 header__control"]')
    const buttonContactUs = await $('.header__content>a[class="cta-button-ui cta-button-ui-23 header__control"]')
    
    await buttonContactUs.waitForDisplayed({
      timeout: 3000,
      reverse: false,
      timeoutMsg: "hola",
      interval: 50
    })
    await buttonContactUs.click();
    const contactUrl = await browser.getUrl();
    expect(contactUrl).toEqual('https://www.epam.com/about/who-we-are/contact')
  })

  // it('change color letter', async() => {
  //   const constumerletter = await $('//span[(text()="Customer")]');
  //   constumerletter.moveTo();
  //   await browser.execute((constumerletter) => {
  //     constumerletter.style.color ="#00f6ff" ;
  //   },constumerletter);
  //   await browser.pause(3000)
  // })

  // it('Learn more', async() => {
  //   const constumer = await $('.section p a[href="/services/client-work"]')
  //   await constumer.click();
  // })
})